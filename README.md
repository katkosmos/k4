# K-4: A Custom 4-bit CPU

The K-4 is a 4-bit data and 8-bit address CPU. It features 14 instructions, with many more being possible to implement through software.

## Instruction Set Architecture

See also `isa.txt`

The K-4 is a little endian (LE), harvard architecture (separate data and program memory) processor.
It has a 4-bit data bus and 8-bit address bus. It also has 4 general purpose registers, named r0-r3.
Operand order for assembly syntax is source, destination; similar to AT&T syntax.

The K-4 has 2 flags: carry (flag 0) and zero (flag 1).

### Notes

Please note that the cycles given below and in `isa.txt` are specific to this emulator, and the ISA does not
specify cycle counts for instructions. More efficient designs are possible. Memory access order is also not
defined.

### Instruction Table

| Format                   | Name                                | Syntax               | Flags (zc) | Cycles |
|--------------------------|-------------------------------------|----------------------|------------|--------|
| 0000 aabb                | ADD with carry                      | ADD A, B             | zc         | 5      |
| 0001 aabb                | AND                                 | AND A, B             | z.         | 5      |
| 0010 aabb                | XOR                                 | XOR A, B             | z.         | 5      |
| 0011 xxaa                | ROR                                 | ROR A                | zc         | 5      |
| 0100 aabb                | Move register to register           | MV A, B              | ..         | 3      |
| 0101 0lhh aaaa aaaa      | Jump and link                       | JAL L, H, A          | ..         | 8      |
| 0101 1xfs                | Set flag to state                   | SET F   or   CLEAR F | zc         | 4      |
| 0110 aaaa raaa           | Load address with register          | LD R, A              | ..         | 4      |
| 0111 aaaa raaa           | Load indirect address with register | LD R, (A)            | ..         | 6      |
| 10rr aaaa aaaa           | Load register with address          | LD A, R              | ..         | 4      |
| 1100 aaaa aaaa           | Jump to address                     | JMP A                | ..         | 4      |
| 1101 aaaa caaa           | Jump to address if condition        | JMP A,C              | ..         | 4      |
| 1110 aaaa aaaa           | Jump to indirect address            | JMP (A)              | ..         | 5      |
| 1111 aaaa raaa           | Load register with indirect address | LD (A), R            | ..         | 6      |

## Installation

Add the following to your project's `Cargo.toml`.

```toml
[dependencies]
k4 = { git = "https://gitlab.com/katkosmos/k4" }
```

## Usage
Read the docs via `cargo doc --open` for documentation on the API.

A quick runthrough is to implement the `System` trait, and then call `CPU::step` with your CPU and `System`-type.

## Contributing

Pull requests are welcome. For major changes, please open an issue first
to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License

[MIT](https://choosealicense.com/licenses/mit/)
