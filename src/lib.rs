//! K-4: A 4-bit CPU
//!
//! This is an emulator for the K-4 CPU, a custom CPU designed around a 4-bit data bus.
//!
//! # Example
//! ```no_run
//! use k4::{System, CPU};
//!
//! // create the system
//! let mut cpu = CPU::new();
//! let mut sys = Sys::new();
//!     
//! // run the cpu
//! loop {
//!     cpu.step(&mut sys);
//! }
//!
//! struct Sys {
//!     prg: [u8; 256],
//!     data: [u8; 256],
//! }
//!
//! impl Sys {
//!     fn new() -> Self {
//!         Self {
//!             // ideally you'd have data in these
//!             prg: std::array::from_fn(|_| 0),
//!             data: std::array::from_fn(|_| 0),
//!         }
//!     }
//! }
//!
//! // implement system bus functions
//! impl System for Sys {
//!     fn read_prg(&mut self, a: u8) -> u8 {
//!         self.prg[a as usize]
//!     }
//!
//!     fn read_data(&mut self, a: u8) -> u8 {
//!         self.data[a as usize]
//!     }
//!     
//!     fn write_data(&mut self, a: u8, d: u8) {
//!         self.data[a as usize] = d;
//!     }
//! }
//! ```

#![deny(missing_docs)]

#![cfg_attr(not(test), no_std)]
#[cfg(test)]
mod tests;

use core::convert::TryFrom;

/// System bus implementation
///
/// This is the system the CPU is hooked up to, and it provides the interface the CPU uses to
/// access memory.
///
/// All values touched by the CPU are masked to 4-bits (lower 4-bits) internally, so there's no
/// need to do so yourself. This includes arguments passed to these functions and their return
/// values.
///
/// # Examples
/// ```
/// use k4::System;
///
/// struct Sys {
///     prg: [u8; 256],
///     data: [u8; 256],
/// }
///
/// impl System for Sys {
///     fn read_prg(&mut self, a: u8) -> u8 {
///         self.prg[a as usize]
///     }
///
///     fn read_data(&mut self, a: u8) -> u8 {
///         self.data[a as usize]
///     }
///     
///     fn write_data(&mut self, a: u8, d: u8) {
///         self.data[a as usize] = d;
///     }
/// }
/// ```
pub trait System {
    /// Read a byte from program memory.
    fn read_prg(&mut self, _a: u8) -> u8 {
        0
    }
    /// Read a byte from data memory.
    fn read_data(&mut self, _a: u8) -> u8 {
        0
    }
    /// Write a byte to data memory.
    fn write_data(&mut self, _a: u8, _d: u8) {}
}

/// Status flags
#[derive(Debug, Clone, Copy, Default, PartialEq, Eq)]
pub struct Flags {
    /// Carry flag, flag 0
    carry: bool,
    /// Zero flag, flag 1
    zero: bool,
}

/// CPU flag
#[derive(Debug, Clone, Copy, PartialEq, Eq)]
enum Flag {
    /// Carry flag, flag 0
    Carry = 0,
    /// Zero flag, flag 1
    Zero = 1,
}

impl TryFrom<u8> for Flag {
    type Error = ();
    #[inline]
    fn try_from(v: u8) -> Result<Self, Self::Error> {
        match v {
            0 => Ok(Flag::Carry),
            1 => Ok(Flag::Zero),
            _ => Err(()),
        }
    }
}

/// CPU State
#[derive(Debug, Clone, PartialEq, Eq, Default)]
enum State {
    /// Fetch the next instruction
    #[default]
    Fetch,
    /// Add with carry
    Add,
    /// AND (Bitwise)
    And,
    /// XOR (Exclusive OR)
    Xor,
    /// Rotate right
    Ror,
    /// Move from one register to another
    Mv,
    /// Decode stage 2 for group 5.
    Group5,
    /// Jump and link
    ///
    /// Stores program counter in registers then jumps
    Jal,
    /// Set flag status
    Flag,
    /// Store register to address
    LdRA,
    /// Store register to indirect address
    LdRI,
    /// Load register from address
    LdAR,
    /// Jump to address
    Jmp,
    /// Jump to address if flag is set
    JmpC,
    /// Jump indirect
    JmpI,
    /// Load register from indirect address
    LdIR,
}

/// CPU
///
/// # Examples
/// ```no_run
/// use k4::{CPU, System};
///
/// // creating a system
/// struct Sys {
///     prg: [u8; 256],
///     data: [u8; 256],
/// }
///
/// impl Sys {
///     fn new() -> Self {
///         Self {
///             prg: std::array::from_fn(|_| 0),
///             data: std::array::from_fn(|_| 0),
///         }
///     }
/// }
///
/// impl System for Sys {
///     fn read_prg(&mut self, a: u8) -> u8 {
///         self.prg[a as usize]
///     }
///
///     fn read_data(&mut self, a: u8) -> u8 {
///         self.data[a as usize]
///     }
///     
///     fn write_data(&mut self, a: u8, d: u8) {
///         self.data[a as usize] = d;
///     }
/// }
///
/// // creating a cpu
/// let mut cpu = CPU::new();
/// let mut sys = Sys::new(); // some type implementing k4::System
///
/// // running
/// loop {
///     cpu.step(&mut sys);
/// }
/// ```
#[derive(Debug, Clone, Default)]
pub struct CPU {
    /// 8-bit PC
    pc: u8,
    /// 4 4-bit registers
    regs: [u8; 4],
    /// Flags
    flags: Flags,
    /// State machine state
    state: State,
    tcu: u8,
    /// Temp storage for instruction processing
    temp: u8,
    /// Temp storage for instruction processing
    temp2: u8,
}

impl CPU {
    /// Creates a new CPU
    #[inline]
    pub fn new() -> Self {
        Self::default()
    }

    /// Resets the CPU
    ///
    /// All this does is set the program counter to 0.
    pub fn reset(&mut self) {
        self.pc = 0;
    }

    /// Returns the current program counter
    #[inline]
    pub fn pc(&self) -> u8 {
        self.pc
    }

    /// Returns the value of r0
    #[inline]
    pub fn r0(&self) -> u8 {
        self.regs[0] & 0xf
    }

    /// Returns the value of r1
    #[inline]
    pub fn r1(&self) -> u8 {
        self.regs[1] & 0xf
    }

    /// Returns the value of r2
    #[inline]
    pub fn r2(&self) -> u8 {
        self.regs[2] & 0xf
    }

    /// Returns the value of r3
    #[inline]
    pub fn r3(&self) -> u8 {
        self.regs[3] & 0xf
    }

    /// Steps the CPU by a cycle.
    ///
    /// # Examples
    /// ```ignore
    /// let mut cpu = CPU::new();
    /// let mut sys = Sys::new(); // some type implementing k4::System
    ///
    /// loop {
    ///     cpu.step(&mut sys);
    /// }
    /// ```
    pub fn step(&mut self, system: &mut dyn System) {
        match self.state {
            State::Fetch => {
                self.tcu = 0;

                let ir = self.read_next_prg(system);

                self.state = match ir {
                    0x0 => State::Add,
                    0x1 => State::And,
                    0x2 => State::Xor,
                    0x3 => State::Ror,
                    0x4 => State::Mv,
                    0x5 => State::Group5,
                    0x6 => State::LdRA,
                    0x7 => State::LdRI,
                    0x8..=0xb => {
                        self.temp2 = ir & 0b11;
                        State::LdAR
                    }
                    0xc => State::Jmp,
                    0xd => State::JmpC,
                    0xe => State::JmpI,
                    0xf => State::LdIR,
                    _ => unreachable!(),
                };
            }
            State::Add => {
                let finished = self.alu_op(system, |cpu, a, b| {
                    let c: u8 = if cpu.flags.carry { 1 } else { 0 };
                    let res = a.wrapping_add(b).wrapping_add(c);
                    (res & 0xf, Some(res > 0xf))
                });

                if finished {
                    self.state = State::Fetch;
                }
            }
            State::And => {
                let finished = self.alu_op(system, |_cpu, a, b| {
                    let res = (a & b) & 0xf;
                    (res, None)
                });

                if finished {
                    self.state = State::Fetch;
                }
            }
            State::Xor => {
                let finished = self.alu_op(system, |_cpu, a, b| {
                    let res = (a ^ b) & 0xf;
                    (res, None)
                });

                if finished {
                    self.state = State::Fetch;
                }
            }
            State::Ror => {
                let finished = self.alu_op(system, |cpu, _a, b| {
                    let a = b;
                    let new_c = (a & 1) != 0;
                    let c = if cpu.flags.carry { 0x8 } else { 0 };
                    let res = ((a >> 1) | c) & 0xf;
                    (res, Some(new_c))
                });

                if finished {
                    self.state = State::Fetch;
                }
            }
            State::Mv => match self.tcu {
                1 => self.temp = self.read_next_prg(system),
                2 => {
                    self.regs[(self.temp & 0b11) as usize] = self.regs[(self.temp >> 2) as usize];
                    self.state = State::Fetch;
                }
                _ => unreachable!(),
            },
            State::Group5 => match self.tcu {
                1 => self.temp = self.read_next_prg(system),
                2 => match self.temp >> 3 {
                    0 => self.state = State::Jal,
                    1 => self.state = State::Flag,
                    _ => unreachable!(),
                },
                _ => unreachable!(),
            },
            State::Jal => match self.tcu {
                3 => self.temp2 = self.read_next_prg(system),
                4 => self.temp2 |= self.read_next_prg(system) << 4,
                5 => {
                    let l = (self.temp >> 2) & 1;
                    self.regs[l as usize] = self.pc & 0xf;
                }
                6 => {
                    let h = self.temp & 0b11;
                    self.regs[h as usize] = self.pc >> 4;
                }
                7 => {
                    self.pc = self.temp2;
                    self.state = State::Fetch;
                }
                _ => unreachable!(),
            },
            State::Flag => {
                // tcu = 3
                let state = self.temp & 1 != 0;
                let flag = Flag::try_from((self.temp & 0b10) >> 1).unwrap();

                match flag {
                    Flag::Carry => self.flags.carry = state,
                    Flag::Zero => self.flags.zero = state,
                }

                self.state = State::Fetch;
            }
            State::LdRA => match self.tcu {
                1 => self.temp = self.read_next_prg(system),
                2 => self.temp |= self.read_next_prg(system) << 4,
                3 => {
                    let r = self.temp >> 7;
                    let a = self.temp & 0x7f;
                    system.write_data(a, self.regs[r as usize]);
                    self.state = State::Fetch;
                }
                _ => unreachable!(),
            },
            State::LdRI => match self.tcu {
                1 => self.temp = self.read_next_prg(system),
                2 => self.temp |= self.read_next_prg(system) << 4,
                3 => self.temp2 = system.read_data((self.temp & 0x7f) << 1) & 0xf,
                4 => self.temp2 |= (system.read_data(((self.temp & 0x7f) << 1) + 1) & 0xf) << 4,
                5 => {
                    let r = self.regs[(self.temp >> 7) as usize];
                    system.write_data(self.temp2, r);
                    self.state = State::Fetch;
                }
                _ => unreachable!(),
            },
            State::LdAR => match self.tcu {
                1 => self.temp = self.read_next_prg(system),
                2 => self.temp |= self.read_next_prg(system) << 4,
                3 => {
                    self.regs[self.temp2 as usize] = system.read_data(self.temp) & 0xf;
                    self.state = State::Fetch;
                }
                _ => unreachable!(),
            },
            State::Jmp => match self.tcu {
                1 => self.temp = self.read_next_prg(system),
                2 => self.temp |= self.read_next_prg(system) << 4,
                3 => {
                    self.pc = self.temp;
                    self.state = State::Fetch;
                }
                _ => unreachable!(),
            },
            State::JmpC => match self.tcu {
                1 => self.temp = self.read_next_prg(system),
                2 => self.temp |= self.read_next_prg(system) << 4,
                3 => {
                    let c: Flag = (self.temp >> 7).try_into().unwrap();
                    let c = match c {
                        Flag::Zero => self.flags.zero,
                        Flag::Carry => self.flags.carry,
                    };

                    let a = self.temp & 0x7f;

                    if c {
                        self.pc = a;
                    }

                    self.state = State::Fetch;
                }
                _ => unreachable!(),
            },
            State::JmpI => match self.tcu {
                1 => self.temp = self.read_next_prg(system),
                2 => self.temp |= self.read_next_prg(system) << 4,
                3 => self.pc = system.read_data(self.temp) & 0xf,
                4 => {
                    self.pc |= (system.read_data(self.temp.wrapping_add(1)) & 0xf) << 4;
                    self.state = State::Fetch;
                }
                _ => unreachable!(),
            },
            State::LdIR => match self.tcu {
                1 => self.temp = self.read_next_prg(system),
                2 => self.temp |= self.read_next_prg(system) << 4,
                3 => self.temp2 = system.read_data((self.temp & 0x7f) << 1) & 0xf,
                4 => self.temp2 |= (system.read_data(((self.temp & 0x7f) << 1) + 1) & 0xf) << 4,
                5 => {
                    self.regs[(self.temp >> 7) as usize] = system.read_data(self.temp2) & 0xf;
                    self.state = State::Fetch;
                }
                _ => unreachable!(),
            },
        }

        self.tcu += 1;
    }

    #[inline]
    fn read_next_prg(&mut self, system: &mut dyn System) -> u8 {
        let b = system.read_prg(self.pc) & 0xf;
        self.pc = self.pc.wrapping_add(1);
        b
    }

    fn alu_op<F: Fn(&mut Self, u8, u8) -> (u8, Option<bool>)>(
        &mut self,
        system: &mut dyn System,
        op: F,
    ) -> bool {
        match self.tcu {
            1 => {
                self.temp = self.read_next_prg(system);
                false
            }
            2 => false, // mv reg op a to alu a
            3 => false, // mv reg op b to alu b
            4 => {
                let a = self.regs[((self.temp >> 2) & 0b11) as usize];
                let b = self.regs[(self.temp & 0b11) as usize];

                let (res, carry) = op(self, a, b);

                if let Some(carry) = carry {
                    self.flags.carry = carry;
                }

                self.flags.zero = res == 0;
                self.regs[(self.temp & 0b11) as usize] = res;
                true
            }
            _ => unreachable!(),
        }
    }
}
