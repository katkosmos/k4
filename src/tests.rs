use super::{Flag, Flags, System, CPU};

struct Sys {
    prg: [u8; 256],
    data: [u8; 256],
}

impl Sys {
    fn new() -> Self {
        Self {
            prg: core::array::from_fn(|_| 0),
            data: core::array::from_fn(|_| 0),
        }
    }
}

// implement system bus functions
impl System for Sys {
    fn read_prg(&mut self, a: u8) -> u8 {
        self.prg[a as usize]
    }

    fn read_data(&mut self, a: u8) -> u8 {
        self.data[a as usize]
    }

    fn write_data(&mut self, a: u8, d: u8) {
        self.data[a as usize] = d;
    }
}

fn make_bundle(code: &[u8], data: &[u8]) -> (CPU, Sys) {
    let cpu = CPU::new();
    let mut sys = Sys::new();

    sys.prg[0..code.len()].copy_from_slice(code);
    sys.data[0..data.len()].copy_from_slice(data);

    (cpu, sys)
}

fn run_code(code: &[u8], data: &[u8], cycles: usize) -> (CPU, Sys) {
    let (mut cpu, mut sys) = make_bundle(code, data);

    for _ in 0..cycles {
        //println!("{:?}", cpu.state);
        cpu.step(&mut sys);
    }

    (cpu, sys)
}

fn cont_code(bundle: (CPU, Sys), cycles: usize) -> (CPU, Sys) {
    let (mut cpu, mut sys) = bundle;

    for _ in 0..cycles {
        //println!("{:?} {:02x} {:02x}", cpu.state, cpu.temp, cpu.temp2);
        cpu.step(&mut sys);
    }

    (cpu, sys)
}

#[test]
fn create_system() {
    let _cpu = CPU::new();
    let _sys = Sys::new();
}

#[test]
fn ldar() {
    let cpu = run_code(
        &[
            0b1000, 0, 0, // ld 0, r0
            0b1001, 1, 0, // ld 1, r1
            0b1010, 2, 0, // ld 2, r2
            0b1011, 3, 0, // ld 3, r3
        ],
        &[0, 1, 2, 3],
        16,
    )
    .0;

    assert_eq!(cpu.r0(), 0);
    assert_eq!(cpu.r1(), 1);
    assert_eq!(cpu.r2(), 2);
    assert_eq!(cpu.r3(), 3);
}

/// Tests an ALU operation using fuzzing.
///
/// Takes a label to identify the operation in error messages, an opcode, and a function
/// defining the operation. The function should take 2 operands and the carry state and
/// return a value where bits 0-3 are the value and having any of bits 4-7 set will mean
/// carry is set.
fn test_alu_op<F: Fn(u8, u8, bool) -> u8>(label: &str, op: u8, f: F) {
    const CONSTANTS: &[u8] = &[1, 2, 0, 0xf];

    for (ai, a) in CONSTANTS.iter().copied().enumerate() {
        for (bi, b) in CONSTANTS.iter().copied().enumerate() {
            for r0i in 0..4 {
                for r1i in 0..4 {
                    for c in 0..2 {
                        let cpu = run_code(
                            &[
                                0b1000 | r0i as u8,
                                ai as u8,
                                (ai >> 8) as u8, // ld \a, \r0
                                0b1000 | r1i as u8,
                                bi as u8,
                                (bi >> 8) as u8, // ld \b, \r1
                                5,
                                0b1000 | c, // set/clear carry
                                op,
                                ((r0i as u8) << 2) | (r1i as u8), // \alu_op \r0, \r1
                            ],
                            CONSTANTS,
                            4 * 3 + 5,
                        )
                        .0;

                        // calculate expected result
                        let c = c != 0;
                        let same = r0i == r1i;
                        let res = if same {
                            f(b, b, c) // b is used here because it's loaded after a
                        } else {
                            f(a, b, c)
                        };

                        // mask to 4-bits and get flags
                        let old_c = c;
                        let c = res > 0xf;
                        let res = res & 0xf;

                        // convert regs to values
                        let r0 = match r0i {
                            0 => cpu.r0(),
                            1 => cpu.r1(),
                            2 => cpu.r2(),
                            3 => cpu.r3(),
                            _ => unreachable!(),
                        };

                        let r1 = match r1i {
                            0 => cpu.r0(),
                            1 => cpu.r1(),
                            2 => cpu.r2(),
                            3 => cpu.r3(),
                            _ => unreachable!(),
                        };

                        // debug info
                        let eq = format!("a={a}, b={b}, c={old_c:?}, A={r0i}, B={r1i} => r0={r0}, r1={r1}, c={c:?}, res={res}");

                        assert_eq!(
                            r0,
                            if same { res } else { a },
                            "{label} doesnt match: A; {eq}"
                        );
                        assert_eq!(r1, res, "{label} doesnt match: result; {eq}");
                        assert_eq!(
                            cpu.flags.zero,
                            res == 0,
                            "{label} doesnt match: zero; {eq}"
                        );
                        assert_eq!(cpu.flags.carry, c, "{label} doesnt match: carry; {eq}");
                    }
                }
            }
        }
    }
}

#[test]
fn add() {
    // a + b + c; c = a+b+c >> 4
    test_alu_op("add", 0, |a, b, c| a + b + if c { 1 } else { 0 });
}

#[test]
fn and() {
    // a & b; c=c (passes carry through)
    test_alu_op("and", 1, |a, b, c| if c { 0x10 } else { 0 } | (a & b));
}

#[test]
fn xor() {
    // a ^ b; c=c (passes carry through)
    test_alu_op("xor", 2, |a, b, c| if c { 0x10 } else { 0 } | (a ^ b));
}

#[test]
fn ror() {
    // (c << 3) | (b >> 1); c=b&1
    test_alu_op("ror", 3, |_, b, c| {
        let cout = (b & 1) << 4;
        let cin = if c { 1 << 3 } else { 0 };
        cout | cin | (b >> 1)
    });
}

#[test]
fn mv() {
    const CONSTANTS: [u8; 5] = [0, 1, 3, 8, 0xf];
    const LD_CYCLES: usize = 4;
    const MV_CYCLES: usize = 3;
    const STEPS: &[[u8; 4]] = &[
        [1, 0, 0, 0], // ld 1, r0 ; initial
        [1, 0, 0, 0], // mv r0, r0
        [1, 1, 0, 0], // mv r0, r1
        [1, 1, 1, 0], // mv r0, r2
        [1, 1, 1, 1], // mv r0, r3
        [1, 2, 1, 1], // ld 2, r1
        [2, 2, 1, 1], // mv r1, r0
        [2, 2, 1, 1], // mv r1, r1
        [2, 2, 2, 1], // mv r1, r2
        [2, 2, 2, 2], // mv r1, r3
        [2, 2, 3, 2], // ld 3, r2
        [3, 2, 3, 2], // mv r2, r0
        [3, 3, 3, 2], // mv r2, r1
        [3, 3, 3, 2], // mv r2, r2
        [3, 3, 3, 3], // mv r2, r3
        [3, 3, 3, 4], // ld 4, r3
        [4, 3, 3, 4], // mv r3, r0
        [4, 4, 3, 4], // mv r3, r1
        [4, 4, 4, 4], // mv r3, r2
        [4, 4, 4, 4], // mv r3, r3
    ];

    let mut bundle = run_code(
        &[
            // init to zero
            0b1000, 0, 0, // ld 0, r0
            0b1001, 0, 0, // ld 0, r1
            0b1010, 0, 0, // ld 0, r2
            0b1011, 0, 0, // ld 0, r3
            0b1000, 1, 0, // 0:  ld 1, r0
            0b0100, 0b0000, // 1:  mv r0, r0
            0b0100, 0b0001, // 2:  mv r0, r1
            0b0100, 0b0010, // 3:  mv r0, r2
            0b0100, 0b0011, // 4:  mv r0, r3
            0b1001, 2, 0, // 5:  ld 2, r1
            0b0100, 0b0100, // 6:  mv r1, r0
            0b0100, 0b0101, // 7:  mv r1, r1
            0b0100, 0b0110, // 8:  mv r1, r2
            0b0100, 0b0111, // 9:  mv r1, r3
            0b1010, 3, 0, // 10: ld 3, r2
            0b0100, 0b1000, // 11: mv r2, r0
            0b0100, 0b1001, // 12: mv r2, r1
            0b0100, 0b1010, // 13: mv r2, r2
            0b0100, 0b1011, // 14: mv r2, r3
            0b1011, 4, 0, // 15: ld 4, r3
            0b0100, 0b1100, // 16: mv r3, r0
            0b0100, 0b1101, // 17: mv r3, r1
            0b0100, 0b1110, // 18: mv r3, r2
            0b0100, 0b1111, // 19: mv r3, r3
        ],
        &CONSTANTS,
        16, // number of cycles for the initial lds
    );

    fn assert_regs(b: &(CPU, Sys), s: (usize, [u8; 4])) {
        let cpu = &b.0;
        let (i, r) = s;
        assert_eq!(cpu.r0(), CONSTANTS[r[0] as usize], "r0 in step {i}");
        assert_eq!(cpu.r1(), CONSTANTS[r[1] as usize], "r1 in step {i}");
        assert_eq!(cpu.r2(), CONSTANTS[r[2] as usize], "r2 in step {i}");
        assert_eq!(cpu.r3(), CONSTANTS[r[3] as usize], "r3 in step {i}");
    }

    let mut steps = STEPS.iter().copied().enumerate();

    // for each grouping
    for _ in 0..4 {
        // execute the load
        let s = steps.next().expect("could not get load step");
        bundle = cont_code(bundle, LD_CYCLES);
        assert_regs(&bundle, s);

        // now execute each move
        for _ in 0..4 {
            let s = steps.next().expect("could not get move step");
            bundle = cont_code(bundle, MV_CYCLES);
            assert_regs(&bundle, s);
        }
    }
}

#[test]
fn flags() {
    const FLAG_CYCLES: usize = 4;
    const STEPS: &[Flags] = &[
        Flags {
            carry: false,
            zero: false,
        },
        Flags {
            carry: false,
            zero: false,
        },
        Flags {
            carry: true,
            zero: false,
        },
        Flags {
            carry: true,
            zero: false,
        },
        Flags {
            carry: true,
            zero: true,
        },
        Flags {
            carry: true,
            zero: false,
        },
        Flags {
            carry: false,
            zero: false,
        },
        Flags {
            carry: false,
            zero: true,
        },
        Flags {
            carry: false,
            zero: false,
        },
        Flags {
            carry: true,
            zero: false,
        },
        Flags {
            carry: true,
            zero: true,
        },
        Flags {
            carry: true,
            zero: false,
        },
        Flags {
            carry: false,
            zero: false,
        },
    ];

    let mut bundle = make_bundle(
        &[
            5, 0b1000, // clear carry
            5, 0b1010, // clear zero
            5, 0b1001, // set carry
            5, 0b1010, // clear zero
            5, 0b1011, // set zero
            5, 0b1010, // clear zero
            5, 0b1000, // clear carry
            5, 0b1011, // set zero
            5, 0b1010, // clear zero
            5, 0b1001, // set carry
            5, 0b1011, // set zero
            5, 0b1010, // clear zero
            5, 0b1000, // clear carry
        ],
        &[],
    );

    // set flags to a known state, we'll be testing how they get set anyways
    bundle.0.flags = Flags {
        carry: false,
        zero: false,
    };

    for (i, step) in STEPS.iter().copied().enumerate() {
        bundle = cont_code(bundle, FLAG_CYCLES);
        assert_eq!(bundle.0.flags, step, "in step {i}");
    }
}

#[test]
#[rustfmt::skip]
fn jmp() {
    assert_eq!(run_code(&[
        0b1100, 0xf, 0xf, // jmp 0xff
    ], &[], 4).0.pc, 0xff);
}

#[test]
fn jmpc() {
    fn check(f: Flag, s: bool, pc: u8) {
        let pc = pc & 0x7f;

        let mut bundle = make_bundle(
            &[
                0b1101,
                pc & 0xf,
                ((pc >> 4) & 7) | ((f as u8) << 3), // jmp 0xff, carry
            ],
            &[],
        );

        match f {
            Flag::Carry => bundle.0.flags.carry = s,
            Flag::Zero => bundle.0.flags.zero = s,
        };

        assert_eq!(
            cont_code(bundle, 4).0.pc,
            match s {
                true => pc,
                false => 0x03,
            }
        );
    }

    check(Flag::Carry, false, 0x7f);
    check(Flag::Carry, true, 0x7f);
    check(Flag::Zero, false, 0x7f);
    check(Flag::Zero, true, 0x7f);
}

#[test]
fn jal() {
    const JAL_CYCLES: usize = 8;
    const JAL_LEN: u8 = 4;
    const JAL: u8 = 0b0101; // technically group 5 but
                            // when bit 7 of the second
                            // bit is cleared, it's JAL, and
                            // here that's kind of implied

    for pc in 0..=0xffu8 {
        for dest in 0..=0xffu8 {
            for l in 0..2 {
                for h in 0..4 {
                    let sb = ((l << 2) | h) as u8;

                    // this needs to be copied in a bit more dynamically
                    let code = [JAL, sb, dest & 0xf, dest >> 4];

                    let mut bundle = make_bundle(&[], &[]);
                    for (i,b) in code.into_iter().enumerate() {
                        let pc = pc.wrapping_add(i as u8);
                        bundle.1.prg[pc as usize] = b;
                    }

                    bundle.0.pc = pc;

                    let bundle = cont_code(bundle, JAL_CYCLES);
                    let cpu = bundle.0;

                    let lv = match l {
                        0 => cpu.r0(),
                        1 => cpu.r1(),
                        _ => unreachable!(),
                    };

                    let hv = match h {
                        0 => cpu.r0(),
                        1 => cpu.r1(),
                        2 => cpu.r2(),
                        3 => cpu.r3(),
                        _ => unreachable!(),
                    };

                    let eq = format!("pc={pc:02x}, dest={dest:02x}, l=r{l}:={lv}, h=r{h}:={hv}; cpu.pc={cpu_pc:02x}", cpu_pc=cpu.pc);
                    let pc = pc.wrapping_add(JAL_LEN);

                    assert_eq!(cpu.pc, dest, "new pc; {eq}");
                    if l != h {
                        assert_eq!(lv, pc & 0xf, "old pc low; {eq}");
                    }
                    assert_eq!(hv, pc >> 4, "old pc high; {eq}");
                }
            }
        }
    }
}

#[test]
fn ldra() {
    fn check(r: u8, a: u8, v: u8) {
        assert!(r < 2);
        assert!(a < 0x80);

        let bundle = run_code(&[
            0b1000 | r, 0, 0, // ld 0, \r
            0b0110, a & 0xf, ((a >> 4) & 7) | (r << 3), // ld \a, \r
        ], &[v], 4+4);

        assert_eq!(bundle.1.data[a as usize], v, "r={r:01x} a={a:02x} v={v:01x}");
    }

    for r in 0..2 {
        for a in 0..=0x7f {
            for v in 0..16 {
                check(r, a, v);
            }
        }
    }
}

#[test]
fn jmpi() {
    const JMP_I_CYCLES: usize = 5;

    fn jmp_i(ja: u8, pa: u8) {
        let code = &[0b1110, pa & 0xf, pa >> 4];

        let mut bundle = make_bundle(code, &[]);
        bundle.1.data[pa as usize] = ja & 0xf;
        bundle.1.data[pa.wrapping_add(1) as usize] = ja >> 4;

        let bundle = cont_code(bundle, JMP_I_CYCLES);
        assert_eq!(bundle.0.pc, ja, "pc");
    }

    for ja in 0..=0xff {
        for pa in 0..=0xff {
            jmp_i(ja, pa);
        }
    }
}
    
#[test]
fn ldir() {
    fn check(r: u8, pa: u8, a: u8, v: u8) {
        assert!(r < 2);
            
        // create code chunk
        let enc_a = pa >> 1;
        let pa = enc_a << 1;
        let mut bundle = make_bundle(&[
            0b1111, enc_a & 0xf, ((enc_a >> 4) & 7) | (r << 3), // ld (\a), \r
        ], &[]);

        // create value at pointer
        bundle.1.data[a as usize] = v;

        // create pointer
        bundle.1.data[pa as usize] = a & 0xf;
        bundle.1.data[pa.wrapping_add(1) as usize] = a >> 4;

        // resync v because it might've been overwritten
        let v = bundle.1.data[a as usize];
            
        // execute with 6 cycles (the len of a load indirect)
        let bundle = cont_code(bundle, 6);

        let rv = match r {
            0 => bundle.0.r0(),
            1 => bundle.0.r1(),
            _ => unreachable!(),
        };

        assert_eq!(rv, v, "r={r:01x} pa={pa:02x} a={a:02x} v={v:01x}");
    }

    for r in 0..2 {
        for pa in 0..=0xff {
            for a in 0..=0xff {
                for v in 0..16 {
                    check(r, pa, a, v);
                }
            }
        }
    }
}

#[test]
fn ldri() {
    fn check(r: u8, pa: u8, a: u8, v: u8) {
        assert!(r < 2);

        let enc_a = pa >> 1;
        let pa = enc_a << 1;
        let mut bundle = make_bundle(&[
            0b1000 | r, 0, 0, // ld 0, \r
            0b0111, enc_a & 0xf, ((enc_a >> 4) & 7) | (r << 3), // ld (\a), \r
        ], &[v]);
            
        // create pointer
        bundle.1.data[pa as usize] = a & 0xf;
        bundle.1.data[pa.wrapping_add(1) as usize] = a >> 4;
            
        // resync v because it might've been overwritten
        let v = bundle.1.data[0];
            
        // execute with 10 cycles (the len of a load + load indirect)
        let bundle = cont_code(bundle, 4+6);

        let rv = match r {
            0 => bundle.0.r0(),
            1 => bundle.0.r1(),
            _ => unreachable!(),
        };

        // check register
        assert_eq!(rv, v, "r={r:01x} pa={pa:02x} a={a:02x} v={v:01x}");
        // check to make sure it was stored
        assert_eq!(bundle.1.data[a as usize], v, "r={r:01x} pa={pa:02x} a={a:02x} v={v:01x}");
    }

    for r in 0..2 {
        for pa in 0..=0xff {
            for a in 0..=0xff {
                for v in 0..16 {
                    check(r, pa, a, v);
                }
            }
        }
    }
}
