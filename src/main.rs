use k4::{System, CPU};

fn main() {
    println!("Hello, world! {}", std::mem::size_of::<CPU>());

    let mut cpu = CPU::new();
    let mut sys = Sys::new();

    //let mut times = Vec::with_capacity(1000000000);
    let mut i = 0;

    //*
    loop {
        //use std::time::{Instant, Duration};
        //let pc = cpu.pc();
        //let (r0, r1, r2, r3) = (cpu.r0(), cpu.r1(), cpu.r2(), cpu.r3());
        //println!("{pc:02x} | {r0:01x} {r1:01x} {r2:01x} {r3:01x}");
        //let cur = Instant::now();
        cpu.step(&mut sys);
        //let elapsed = cur.elapsed().as_nanos();
        //times.push(elapsed);

        i += 1;
        if i >= 1000000000 {
            break;
        }

        //if times.len() == times.capacity() {
        //break;
        //}
    }
    // */

    //println!("{}", times.iter().sum::<u128>() as f64 / times.len() as f64);
}

#[derive(Debug, Clone)]
struct Sys {
    pub prg: [u8; 256],
    pub data: [u8; 256],
}

impl Sys {
    pub fn new() -> Self {
        let mut sys = Self {
            prg: std::array::from_fn(|_| 0),
            data: std::array::from_fn(|_| 0),
        };

        // constants
        sys.data[0] = 0;
        sys.data[1] = 1;

        // program
        sys.prg[0] = 0b1000; // ld 0, r0
        sys.prg[1] = 0x0;
        sys.prg[2] = 0x0;
        sys.prg[3] = 0b1001; // ld 1, r1
        sys.prg[4] = 0x1;
        sys.prg[5] = 0x0;
        sys.prg[6] = 0x5; // clear carry
        sys.prg[7] = 0b1000;
        sys.prg[8] = 0x0; // add r1, r0
        sys.prg[9] = 0b0100;
        sys.prg[10] = 0xc; // jmp 6
        sys.prg[11] = 0x6;
        sys.prg[12] = 0x0;

        sys
    }
}

impl System for Sys {
    fn read_prg(&mut self, a: u8) -> u8 {
        self.prg[a as usize]
    }

    fn read_data(&mut self, a: u8) -> u8 {
        self.data[a as usize]
    }

    fn write_data(&mut self, a: u8, d: u8) {
        self.data[a as usize] = d;
    }
}
